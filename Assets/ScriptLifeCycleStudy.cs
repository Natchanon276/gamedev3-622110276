using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptLifeCycleStudy : MonoBehaviour
{
    private void Awake()
    {
        Debug.Log("Awake()has been called.");
    }

    private void Start()
    {
        Debug.Log("Start() has been called.");
    }

    private void Update()
    {
        
    }

    private void OnDisable()
    {
        Debug.Log(("OnDestroy() has been called."));
    }

    private void OnApplicationPause()
    {
        Debug.Log("OnApplicationQuit() has been called.");
    }
}
