using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCube : MonoBehaviour
{
    public float _movementStep;

    void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            this.transform.Translate(-_movementStep,0,0);
        } else if (Input.GetKey(KeyCode.RightArrow))
        {
            this.transform.Translate(_movementStep,0,0);
        }
    }
}
