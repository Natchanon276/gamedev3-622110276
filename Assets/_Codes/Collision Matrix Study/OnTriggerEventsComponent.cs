using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTriggerEventsComponent : MonoBehaviour, IHasOriginalColour
{
    Color _originalColour;

    void Start()
    {
        _originalColour = this.GetComponent<MeshRenderer>().materials[0].color;
    }

    void Update()
    {

    }

    public Color GetOriginalColour()
    {
        return this._originalColour;
    }

    private void OnTriggerEnter(Collider collider)
    {
        this.GetComponent<MeshRenderer>().materials[0].color = Color.yellow;
    }

    private void OnTriggerStay(Collider collider)
    {

    }

    private void OnTriggerExit(Collider collider)
    {
        this.GetComponent<MeshRenderer>().materials[0].color = _originalColour;
    }
}
