using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemTypeComponent : MonoBehaviour
{
    [SerializeField] 
    protected ItemType _itemTpye;

    public ItemType Type
    {
        get
        {
            return _itemTpye;
        }
        set
        {
            _itemTpye = value;
        }
    }
}
