using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlByInputManager : MonoBehaviour
{
    public float _horizontalMovementScale = 0.5f;

    public float _vertialMovementScale = 0.5f;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Translate(new Vector3(Input.GetAxisRaw("Horizontal")*_horizontalMovementScale,Input.GetAxisRaw("Vertical")*_vertialMovementScale,0));
    }
}
