using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlObjectUsingMouse : MonoBehaviour
{
    private Vector3 _mousePrePositon;

    public float _mouseDeltaVectorScaling = 0.5f;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 mouseCurrentPos = Input.mousePosition;
        Vector3 mouseDeltaVector = Vector3.zero;
        mouseDeltaVector = (mouseCurrentPos - _mousePrePositon).normalized;
        
        if (Input.GetMouseButtonUp(0))
        {
            this.transform.Translate(mouseDeltaVector*_mouseDeltaVectorScaling,Space.World);
        }
        this.transform.Translate(0,0,Input.mouseScrollDelta.y*_mouseDeltaVectorScaling,Space.World);
        
        if (Input.GetMouseButtonDown(1))
        {
            this.transform.Translate(mouseDeltaVector*_mouseDeltaVectorScaling,Space.World);
        }
        if (Input.GetMouseButton(2))
        {
            this.transform.Translate(mouseDeltaVector*_mouseDeltaVectorScaling,Space.World);
        }
        _mousePrePositon = mouseCurrentPos;
        
    }
}
